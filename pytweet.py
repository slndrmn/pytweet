# -*- coding: utf-8 -*-

from twython import Twython
import sys
import os
import json

# open key file or exit if file does not exist
try:
    key_file = open(__file__[:-len(os.path.basename(__file__))] + "keys.json", "r")
except FileNotFoundError as error:
    print("[ERROR] Missing key file")
    exit(1)

keys = json.loads(key_file.read())

# exit if keys are incomplete
if len(keys) != 4:
    print("[ERROR] Invalid key file. Make sure that all 4 needed keys are included and seperated by line breaks.")
    exit(1)

# check if each key is at least 20 chars long
for key, value in keys.items():
    if len(value) < 20:
        print("[ERROR] Your key file contains at least one invalid key. Please reconfigure it.")
        exit(1)

# tl;dr if a pipe ("|") is used, use this as input, else check argv[1] or ask for it
if not sys.stdin.isatty():
    content = sys.stdin.read()[:-1]
else:
    if len(sys.argv) > 1:
        content = sys.argv[1]
    else:
        content = input("Tweet: ")

# map keys and create Twitter client
CONSUMER_KEY = keys["CONSUMER_KEY"]
CONSUMER_SECRET = keys["CONSUMER_SECRET"]
ACCESS_KEY = keys["ACCESS_KEY"]
ACCESS_SECRET = keys["ACCESS_SECRET"]

# initialize Twitter client and send (and print) tweet
twitter_client = Twython(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_KEY, ACCESS_SECRET)
twitter_client.update_status(status = content)
print("Tweet sent:\n" + content)