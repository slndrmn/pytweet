# pyTweet

Ein Python-Skript zum (automatisierten) Senden eines Tweets über eine eigene Twitter-App.

## Vorbereitungen
  
* Installation von Python 3.6 (sofern noch nicht vorhanden)
* Installation von Twython mittels `pip`:  
`pip install twython`
* Anfordern eines [Twitter-Developer-Accounts](https://apps.twitter.com "Twitter Developer Dashboard") und Anlegen einer **eigenen** App mit Schreib- und Leserechten für den eigenen Account (das ist inzwischen ziemlich kompliziert, da man ne ganze Menge angeben muss)
* Generation von API Tokens im Twitter Developer Dashboard

Nach diesen Vorkehrungen kann das Tool verwendet werden.

## Setup

Zunächst müssen die API-Keys in einer Datei namens `keys.json` gespeichert werden. Diese muss sich im gleichen Verzeichnis wie die Datei `pytweet.py` befinden und folgende Struktur aufweisen (die Anzahl der x stimmt nicht zwangsweise mit der tatsächlichen Anzahl an Zeichen überein):

```json
{
    "CONSUMER_KEY": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
    "CONSUMER_SECRET": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
    "ACCESS_KEY": "xxxxxxxx-xxxxxxxxxxxxxxxxxxxxxxxx",
    "ACCESS_SECRET": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
}
```

Diese Datei kann manuell angelegt werden. Die Formatierung spielt dabei keine Rolle. Alternativ steht ein weiteres Skript zur Verfügung, welches die Datei automatisch erstellt. Es kann mit `python config.py` ausgeführt werden und fragt dann nacheinander nach den 4 Keys.

## Verwendung

### Syntaxbeschreibung

`python pytweet.py [<tweet>]`

Wird kein Tweet übergeben (wichtig: in Anführungszeichen!), dann wird nach dem Start des Skripts nach diesem gefragt. Es ist außerdem möglich, aus dem STDIN-Stream von unixoiden Betriebssystemen zu lesen ("Pipelining").